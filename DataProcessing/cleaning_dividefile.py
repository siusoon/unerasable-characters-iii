# -*- coding: utf-8 -*-
'''
credit: King-wa Fu & Yuner Zhu (2020) Did the world overlook the media’s early warning of COVID-19?, Journal of Risk Research, DOI: 10.1080/13669877.2020.1756380
https://figshare.com/articles/COVID-19_related_Weibo_Data_from_Did_the_world_overlook_the_media_s_early_warning_of_COVID-19_/12199038
Between December 1, 2019 and February 27, 2020, Weiboscope collected 11,362,502 posts,
among which 1,230,353 contain at least an outbreak-related keyword (please refer to the paper)
and 2,104 (1.7 per 1,000) have been censored.

Data fields:
1: "created_at": date of publication
2: "text_cleaned": text body with all @XXX mentions removed
- to run, execute:  python filename.py

actions:
- 2020-01-19T09:52:22Z -> replace the last character 'Z' from the timestamp to ' <del>' (space + the opening html tag)
- save as a seperate file
- defined the white list and prepare to add tags
- sort the data according to the timestamp
- combine column and rows
  - check against the white_list (+ special characters handling <>/ not in '</del>')

last update: 30 Mar 2021
'''
import pandas as pd
import numpy as np
import string
import re

originalsrc = 'generateddata/cleaned_COVID_censored.txt'
filepath1 = 'generateddata/cleaned_1.txt'
filepath2 = 'generateddata/cleaned_2.txt' #final result for use

df = pd.read_csv(originalsrc, engine='python', usecols=['created_at','text_cleaned'])
df.sort_values(by=['created_at'], inplace=True) #sort the date
df.created_at= "</del><timestamp>" + df['created_at']
df.created_at=df.created_at.str.slice_replace(36, repl=' </timestamp><del>') #25

# checking on specific context: this dataset and the characters that are found in this dataset

white_list=['!','(',')','-','[',']','{','}','，',';',':','\'','"','\\','.','?','@','#','$','%','^','&','*','_','~',',','。','：','【','】','“','”','；','？','《',' ','》','（','）','『','』','！','、','→','…','🔹','🙈','🐎','⬇️','🍳','🐂','👉🏻','➡️','🥶','😱','💔','💩','🕯️','😑','😓','❓','🙃','🔥','💰','🍊','🙏','👏','😡']

# special handle '<'
df.text_cleaned=df.text_cleaned.str.replace(r'<(?!/)', '</del><<del>', regex=True) # find symbol < with the next character not equal to /
# special handle '>'
df.text_cleaned=df.text_cleaned.str.replace(r'(?!l)>', '</del>><del>', regex=True) # find symbol > with the previous character not equal to 'l'
# special handle '/'
df.text_cleaned=df.text_cleaned.str.replace(r'/(?!del)', '</del>/<del>', regex=True) # find symbol / with the next character not equal to /

for x in white_list:
    df.text_cleaned = df.text_cleaned.str.replace(x, "</del>" + x + "<del>") #check characters

df = df['created_at'].map(str) + df['text_cleaned'].map(str)
#df = df['text_cleaned'].map(str)
df.to_csv(filepath1, encoding='utf-8', index=False) #without the index column
print(df.head(100))

# combine all rows
data=open(filepath1).readlines()
for n,line in enumerate(data):
    if line.startswith("line"):
       data[n] = "\n"+line.rstrip()
    else:
       data[n]=line.rstrip()
arrayStr = ''.join(data)
#clean tags with duplication
arrayStr=arrayStr.replace('<del>​</del>', '') #delete tags found in duplicate whitelist (there is a special character in between)

with open(filepath2, 'w+b') as f:
    f.write(arrayStr.encode())
