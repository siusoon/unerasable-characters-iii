# Unerasable Characters III

![](https://gitlab.com/siusoon/unerasable-characters-iii/-/raw/master/visual/unnerasablecharactersIII.gif)

## Description

Unerasable Characters III utilizes data between 1 December 2019 and 27 February 2020, the time when the COVID-19 outbreak was started in China. According to King-wa Fu & Yuner Zhu, there were 11,362,502 posts during the period, among which 1,230, 353 contain at least an outbreak-related keyword and 2,104 (1.7 per 1,000) posts had been censored.

The artwork displays all the erased archives in the format of a web presentation, where each tweet is unreadable. The content has either been obscured or blacked out, except the punctuation​, emojis and special characters. However, what remain are the pauses and blurry timestamps, depicting the affective and expressive, as well as temporal and spatial dimensions of unheard voices. Users can interact with the web by pointing to those pauses, contemplating the poetics of silence and erasure, and further questioning how the culture is being normalized via systematic processes and political infrastructure.

See the full description here: http://siusoon.net/unerasable-characters-iii/

## Files

### 1. Data Source 

- King-wa Fu & Yuner Zhu (2020) Did the world overlook the media’s early warning of COVID-19?, Journal of Risk Research, DOI: 10.1080/13669877.2020.1756380
https://figshare.com/articles/COVID-19_related_Weibo_Data_from_Did_the_world_overlook_the_media_s_early_warning_of_COVID-19_/12199038
    - Between December 1, 2019 and February 27, 2020, Weiboscope collected 11,362,502 posts, among which 1,230,353 contain at least an outbreak-related keyword (please refer to the paper) and 2,104 (1.7 per 1,000) have been censored.

Thanks to Dr. Fu King-wa and the Weiboscope research project 

### 2. Parsing the data source 

- First you need to extract those with only censored data i.e 2104 records
- Next is to parse the data so as to insert the special html tags for later use (see the source file comment)
[Python file](https://gitlab.com/siusoon/unerasable-characters-iii/-/blob/master/DataProcessing/cleaning_dividefile.py)

### 3. Web page 

A static html simply display the unerasable characters with customized styling and interactivity.

Best view on a desktop computer.

See the source [here](https://gitlab.com/siusoon/unerasable-characters-iii/-/tree/master/WebPage)

See the result

## Acknowledgements

Weiboscope research project, Dr. Fu King-wa, Polly Poon, as well as Joel Kwong, Florence Wai and Jason Lam from the Microwave commissioned project – Connecting the Dots

## Notes

Relate to [Unerasable Characters II](https://github.com/siusoon/UnerasableCharactersII)
